/*
 * GenConfigurator
 * Copyright (C) Floppy012 <https://f012.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package dev.f012.genconfigurator;

import dev.f012.genconfigurator.command.BaseCommand;
import dev.f012.genconfigurator.generator.ConfigurableGenerator;
import dev.f012.genconfigurator.generator.CustomGeneratorInjector;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import org.bukkit.command.CommandSender;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main class of this plugin.
 */
public class GenConfigurator extends JavaPlugin {

	private static final String[] ALLOWED_VERSIONS = { "1.13.2-R0.1-SNAPSHOT" };

	public static GenConfigurator instance;
	private final Set<ConfigurableGenerator> activeGenerators = new HashSet<>();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onEnable() {
		instance = this;
		this.loadConfig(false);
		this.ensureAllowedVersion();
		this.getCommand("genconfigurator").setExecutor(new BaseCommand());
	}

	/**
	 * Registers a generator to the central generators set.
	 *
	 * @param gen the generator to register.
	 */
	public void registerGenerator(ConfigurableGenerator gen) {
		this.activeGenerators.add(gen);
	}

	/**
	 * Reloads the configuration file and updates all active generators.
	 */
	@Override
	public void reloadConfig() {
		this.loadConfig(true);
		this.activeGenerators.forEach(ConfigurableGenerator::updateConfig);
		this.getLogger().info("Configuration reloaded!");
	}

	/**
	 * Loads the configuration and immediately saves it to add possible missing values.
	 *
	 * @param reload whether the config is reloaded. If true is provided the config will not be saved after it has been loaded.
	 */
	private void loadConfig(boolean reload) {
		try {
			Config.load();
			if (!reload)
				Config.save();
		} catch (IOException ex) {
			this.getLogger().log(Level.SEVERE, "Error while loading config! Shutting down the server to prevent further damage ...", ex);
			this.getServer().shutdown();
		}
	}

	/**
	 * Ensures that the current version of the server is compatible with this plugin.
	 */
	private void ensureAllowedVersion() {
		String currentVersion = this.getServer().getBukkitVersion();
		for (String allowed : ALLOWED_VERSIONS) {
			if (currentVersion.equalsIgnoreCase(allowed))
				return;
		}

		this.getLogger().severe("The current Server version is not allowed! Cannot proceed without risking world corruption! Shutting down ...");
		this.getServer().shutdown();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
		return new CustomGeneratorInjector();
	}

	/**
	 * Sends a prefixed message to the provided {@link CommandSender}.
	 *
	 * @param recipient the {@link CommandSender} to send the message to.
	 * @param message   the message to send.
	 */
	public static void sendMessage(CommandSender recipient, String message) {
		sendMessage(recipient, message, true);
	}

	/**
	 * Sends a message to the provided {@link CommandSender}.
	 *
	 * @param recipient the {@link CommandSender} to send the message to.
	 * @param message   the message to send.
	 * @param prefix    whether to prefix the message with the plugin's prefix.
	 */
	public static void sendMessage(CommandSender recipient, String message, boolean prefix) {
		if (prefix)
			message = "§8[§3GenConfigurator§8]§r " + message;
		recipient.sendMessage(message);
	}


}
