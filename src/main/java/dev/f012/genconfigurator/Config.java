/*
 * GenConfigurator
 * Copyright (C) Floppy012 <https://f012.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package dev.f012.genconfigurator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * Configuration model for the configuration of this plugin.
 */
public class Config {

	private static final File CONFIG_FILE = new File(GenConfigurator.instance.getDataFolder(), "config.yml");

	public static final Map<String, WorldConfiguration> WORLDS = new HashMap<>();

	/**
	 * Loads the configuration. If the configuration file is non-existent. The default data will be saved first.
	 *
	 * @throws IOException if an I/O error occurs during saving.
	 * @see Config#save()
	 */
	static void load() throws IOException {
		if (!CONFIG_FILE.exists()) {
			save();
			return;
		}
		YamlConfiguration config = YamlConfiguration.loadConfiguration(CONFIG_FILE);

		ConfigurationSection section = config.getConfigurationSection("worlds");
		if (section != null) {
			for (String world : section.getKeys(false)) {
				WORLDS.put(world, WorldConfiguration.load(section.getConfigurationSection(world)));
			}
		}

	}

	/**
	 * Saves the configuration.
	 *
	 * @throws IOException if an I/O error occurs during the write operation.
	 */
	static void save() throws IOException {
		YamlConfiguration config = new YamlConfiguration();

		ConfigurationSection worldsSection = config.createSection("worlds");
		for (String world : WORLDS.keySet()) {
			WORLDS.get(world).save(worldsSection.createSection(world));
		}

		config.save(CONFIG_FILE);
	}

	/**
	 * Model for a world-based configuration.
	 */
	@Getter
	@Setter
	public static class WorldConfiguration {

		private BiomeConfiguration global;
		private Map<String, BiomeConfiguration> biomes;

		/**
		 * Loads the configuration from a {@link ConfigurationSection} into a new {@link WorldConfiguration} object.
		 *
		 * @param section the {@link ConfigurationSection} to load the configuration from.
		 *
		 * @return the {@link WorldConfiguration} object containing the configuration.
		 */
		private static WorldConfiguration load(ConfigurationSection section) {
			WorldConfiguration config = new WorldConfiguration();
			ConfigurationSection globalSection = section.getConfigurationSection("global");
			if (globalSection != null) {
				config.global = BiomeConfiguration.load(globalSection);
			}

			ConfigurationSection biomesSection = section.getConfigurationSection("biomes");
			if (biomesSection != null) {
				config.biomes = new HashMap<>();
				for (String biome : biomesSection.getKeys(false)) {
					config.biomes.put(biome, BiomeConfiguration.load(biomesSection.getConfigurationSection(biome)));
				}
			}

			return config;
		}

		/**
		 * Saves the configuration stored in this object into the provided {@link ConfigurationSection}.
		 *
		 * @param section the {@link ConfigurationSection} to save the configuration to.
		 */
		private void save(ConfigurationSection section) {
			if (this.global != null) {
				ConfigurationSection globalSection = section.createSection("global");
				this.global.save(globalSection);
			}

			if (this.biomes != null) {
				ConfigurationSection biomesSection = section.createSection("biomes");
				for (String biome : this.biomes.keySet()) {
					this.biomes.get(biome).save(biomesSection.createSection(biome));
				}
			}
		}

	}

	/**
	 * Model for the configuration of biome-based values. This model is also used for the world-wide configuration.
	 */
	@Getter
	@Setter
	public static class BiomeConfiguration {

		private Map<String, VeinConfiguration> veins;

		/**
		 * Loads the configuration from a {@link ConfigurationSection} into a new {@link BiomeConfiguration} object.
		 *
		 * @param section the {@link ConfigurationSection} to load the configuration from.
		 *
		 * @return the {@link BiomeConfiguration} object containing the configuration.
		 */
		private static BiomeConfiguration load(ConfigurationSection section) {
			BiomeConfiguration config = new BiomeConfiguration();
			if (section == null)
				return config;

			config.veins = new HashMap<>();
			for (String vein : section.getKeys(false)) {
				config.veins.put(vein, VeinConfiguration.load(section.getConfigurationSection(vein)));
			}

			return config;
		}

		/**
		 * Saves the configuration stored in this object into the provided {@link ConfigurationSection}.
		 *
		 * @param section the {@link ConfigurationSection} to save the configuration to.
		 */
		private void save(ConfigurationSection section) {
			if (this.veins == null || this.veins.isEmpty())
				return;

			for (String key : this.veins.keySet()) {
				ConfigurationSection veinSection = section.createSection(key);
				this.veins.get(key).save(veinSection);
			}
		}

	}

	/**
	 * Model for a single vein configuration.
	 */
	public static class VeinConfiguration {

		private boolean enable;
		private int maxVeinSize;
		private int minY;
		private int maxY;
		private int iterations;

		/**
		 * Loads the configuration from a {@link ConfigurationSection} into a new {@link VeinConfiguration} object.
		 *
		 * @param section the {@link ConfigurationSection} to load the configuration from.
		 *
		 * @return the {@link VeinConfiguration} object containing the configuration.
		 */
		private static VeinConfiguration load(ConfigurationSection section) {
			if (section == null)
				return null;

			VeinConfiguration config = new VeinConfiguration();
			config.enable = section.getBoolean("enable", true);
			config.maxVeinSize = section.getInt("maxVeinSize", 9);
			config.minY = section.getInt("minY", 0);
			config.maxY = section.getInt("maxY", 128);
			config.iterations = section.getInt("iterations", 10);

			return config;
		}

		/**
		 * Saves the configuration stored in this object into the provided {@link ConfigurationSection}.
		 *
		 * @param section the {@link ConfigurationSection} to save the configuration to.
		 */
		public void save(ConfigurationSection section) {
			section.set("enable", this.enable);
			section.set("maxVeinSize", this.maxVeinSize);
			section.set("minY", this.minY);
			section.set("maxY", this.maxY);
			section.set("iterations", this.iterations);
		}

		/**
		 * Returns whether this vein-type should be generated.
		 *
		 * @return true if the vein should be generated, otherwise false.
		 */
		public boolean isEnabled() {
			return enable;
		}

		/**
		 * Sets whether this vein.type should be generated.
		 *
		 * @param enable true if the vein should be generated, otherwise false.
		 */
		public void setEnable(boolean enable) {
			this.enable = enable;
		}

		/**
		 * Returns the maximum amount of ores that a vein of this type can contain.
		 *
		 * @return an integer.
		 */
		public int getMaxVeinSize() {
			return maxVeinSize;
		}

		/**
		 * Sets the maximum amount of ores that a vein of this type can contain.
		 *
		 * @param maxVeinSize the maximum amount of ores.
		 */
		public void setMaxVeinSize(int maxVeinSize) {
			this.maxVeinSize = maxVeinSize;
		}

		/**
		 * Returns the minimal Y-Level at which a vein of this type can generate.
		 *
		 * @return an integer.
		 */
		public int getMinY() {
			return minY;
		}

		/**
		 * Sets the minimal Y-Level at which a vein of this type can generate.
		 *
		 * @param minY the minimum Y-Level at which a vein of this type can generate.
		 */
		public void setMinY(int minY) {
			this.minY = minY;
		}

		/**
		 * Returns the maximum Y-Level at which a vein of this type can generate.
		 *
		 * @return an integer.
		 */
		public int getMaxY() {
			return maxY;
		}

		/**
		 * Sets the maximum Y-Level at which a vein of this type can generate.
		 *
		 * @param maxY the maximum Y-Level at which a vein of this type can generate.
		 */
		public void setMaxY(int maxY) {
			this.maxY = maxY;
		}

		/**
		 * Gets the amount of iterations per chunk for a vein of this type.
		 *
		 * @return an integer.
		 */
		public int getIterations() {
			return iterations;
		}

		/**
		 * Sets the amount of iterations per chunk for a vein of this type.
		 *
		 * @param iterations the iterations per chunk.
		 */
		public void setIterations(int iterations) {
			this.iterations = iterations;
		}


	}

}
