/*
 * GenConfigurator
 * Copyright (C) Floppy012 <https://f012.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package dev.f012.genconfigurator.biomes;

import dev.f012.genconfigurator.Config.BiomeConfiguration;
import dev.f012.genconfigurator.Config.VeinConfiguration;
import dev.f012.genconfigurator.GenConfigurator;
import dev.f012.genconfigurator.Util;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import net.minecraft.server.v1_13_R2.BiomeBase;
import net.minecraft.server.v1_13_R2.BiomeForestHills;
import net.minecraft.server.v1_13_R2.BiomeTaiga;
import net.minecraft.server.v1_13_R2.Block;
import net.minecraft.server.v1_13_R2.IBlockData;
import net.minecraft.server.v1_13_R2.MinecraftKey;
import net.minecraft.server.v1_13_R2.RegistryBlocks;
import net.minecraft.server.v1_13_R2.WorldGenFeatureChanceDecoratorCountConfiguration;
import net.minecraft.server.v1_13_R2.WorldGenFeatureComposite;
import net.minecraft.server.v1_13_R2.WorldGenFeatureOreConfiguration;
import net.minecraft.server.v1_13_R2.WorldGenStage;
import net.minecraft.server.v1_13_R2.WorldGenStage.Decoration;
import net.minecraft.server.v1_13_R2.WorldGenSurfaceComposite;
import net.minecraft.server.v1_13_R2.WorldGenerator;

/**
 * Represents a biome that is overridden by the configuration.
 */
public class OverriddenBiome extends BiomeBase {

	private static final Field GEN_FEATURE_B;
	private static final Map<String, IBlockData> BLOCK_CACHE = new HashMap<>();

	static {
		try {
			GEN_FEATURE_B = WorldGenFeatureComposite.class.getDeclaredField("b");
			GEN_FEATURE_B.setAccessible(true);
		} catch (NoSuchFieldException | SecurityException ex) {
			throw new RuntimeException("Could not get required field!", ex);
		}
	}

	/**
	 * Constructs a new Overridden biome by copying all values from the provided biome and modifying them to match the provided configuration.
	 *
	 * @param extend the Biome (extending {@link BiomeBase} to override.
	 * @param config the {@link BiomeConfiguration} to use for this biome.
	 */
	public OverriddenBiome(BiomeBase extend, BiomeConfiguration config) {
		//Required to prevent NullPointerExceptions. Data provided here doesn't matter since it will be overwritten.
		super((new BiomeBase.a()).a(new WorldGenSurfaceComposite<>(BiomeForestHills.au, BiomeForestHills.ai)).a(BiomeBase.Precipitation.RAIN).a(BiomeBase.Geography.FOREST).a(0.45F).b(0.3F).c(0.7F).d(0.8F).a(4159204).b(329011).a((String) null));

		Field[] declaredFields = BiomeBase.class.getDeclaredFields();

		//Copy all data from the instance to be copied to this instance
		try {
			for (int fieldN = 0; fieldN < declaredFields.length; fieldN++) {
				Field field = declaredFields[fieldN];
				if (Modifier.isStatic(field.getModifiers()))
					continue;
				field.setAccessible(true);
				if (Modifier.isFinal(field.getModifiers()))
					Util.definalize(field);

				Object value = field.get(extend);

				if (value instanceof Map) {
					Object thisValue = field.get(this);
					((Map) thisValue).putAll((Map) value);
				} else if (value instanceof Collection) {
					Object thisValue = field.get(this);
					((Collection) thisValue).addAll((Collection) value);
				} else if (value instanceof Set) {
					Object thisValue = field.get(this);
					((Set) thisValue).addAll((Set) value);
				} else {
					field.set(this, value);
				}

			}
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException ex) {
			throw new RuntimeException("Error during reflection field copy!", ex);
		}

		if (config != null) {

			List<WorldGenFeatureComposite<?, ?>> composites = this.aW.get(Decoration.UNDERGROUND_ORES);
			Iterator<WorldGenFeatureComposite<?, ?>> iterator = composites.iterator();
			try {
				while (iterator.hasNext()) {
					WorldGenFeatureComposite<?, ?> composite = iterator.next();
					Object b = GEN_FEATURE_B.get(composite);
					if (b.getClass() != WorldGenFeatureOreConfiguration.class)
						continue;

					WorldGenFeatureOreConfiguration oreConfig = (WorldGenFeatureOreConfiguration) b;
					IBlockData blockData = oreConfig.d;
					String blockname = RegistryBlocks.BLOCK.getKey(blockData.getBlock()).toString();

					if (config.getVeins().containsKey(blockname)) {
						BLOCK_CACHE.put(blockname, blockData);
						iterator.remove();
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException ex) {
				throw new RuntimeException("Erorr while getting field values!", ex);
			}

			for (String blockname : config.getVeins().keySet()) {
				IBlockData data = BLOCK_CACHE.get(blockname);
				if (data == null) {
					Block block = RegistryBlocks.BLOCK.get(new MinecraftKey(blockname));
					if (block == null) {
						GenConfigurator.instance.getLogger().log(Level.WARNING, "Could not find block by name {0}", blockname);
						continue;
					}
					data = block.getBlockData();

					BLOCK_CACHE.put(blockname, data);
				}

				VeinConfiguration veinConfig = config.getVeins().get(blockname);
				if (!veinConfig.isEnabled())
					continue;

				this.a(WorldGenStage.Decoration.UNDERGROUND_ORES, a(WorldGenerator.an, new WorldGenFeatureOreConfiguration(WorldGenFeatureOreConfiguration.a, data, veinConfig.getMaxVeinSize()), BiomeTaiga.t, new WorldGenFeatureChanceDecoratorCountConfiguration(veinConfig.getIterations(), veinConfig.getMinY(), veinConfig.getMinY(), veinConfig.getMaxY())));
			}
		}
	}

}
