/*
 * GenConfigurator
 * Copyright (C) Floppy012 <https://f012.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package dev.f012.genconfigurator;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Util {

	/**
	 * Removes the {@link Modifier#FINAL} modifier from the modifiers of the provided {@link Fileld}.
	 *
	 * @param f the field to remove the {@link Modifier#FINAL} from.
	 *
	 * @throws IllegalArgumentException by {@link Field#setInt(java.lang.Object, int) }
	 * @throws NoSuchFieldException     by {@link Class#getDeclaredField(java.lang.String) }
	 * @throws IllegalAccessException   by {@link Field#setInt(java.lang.Object, int) }
	 */
	public static void definalize(Field f) throws IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
		Field modifiers = Field.class.getDeclaredField("modifiers");
		modifiers.setAccessible(true);
		modifiers.setInt(f, f.getModifiers() & ~Modifier.FINAL);
	}

}
