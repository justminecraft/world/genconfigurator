/*
 * GenConfigurator
 * Copyright (C) Floppy012 <https://f012.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package dev.f012.genconfigurator.generator;

import dev.f012.genconfigurator.Config;
import dev.f012.genconfigurator.Config.BiomeConfiguration;
import dev.f012.genconfigurator.Config.WorldConfiguration;
import dev.f012.genconfigurator.biomes.OverriddenBiome;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.minecraft.server.v1_13_R2.BiomeBase;
import net.minecraft.server.v1_13_R2.BlockFalling;
import net.minecraft.server.v1_13_R2.BlockPosition;
import net.minecraft.server.v1_13_R2.ChunkGenerator;
import net.minecraft.server.v1_13_R2.EnumCreatureType;
import net.minecraft.server.v1_13_R2.GeneratorSettings;
import net.minecraft.server.v1_13_R2.IChunkAccess;
import net.minecraft.server.v1_13_R2.IRegistry;
import net.minecraft.server.v1_13_R2.RegionLimitedWorldAccess;
import net.minecraft.server.v1_13_R2.SeededRandom;
import net.minecraft.server.v1_13_R2.StructureGenerator;
import net.minecraft.server.v1_13_R2.StructureStart;
import net.minecraft.server.v1_13_R2.World;
import net.minecraft.server.v1_13_R2.WorldChunkManager;
import net.minecraft.server.v1_13_R2.WorldGenFeatureComposite;
import net.minecraft.server.v1_13_R2.WorldGenFeatureConfiguration;
import net.minecraft.server.v1_13_R2.WorldGenStage;
import org.bukkit.craftbukkit.libs.it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import org.bukkit.craftbukkit.libs.it.unimi.dsi.fastutil.longs.LongSet;
import org.bukkit.craftbukkit.v1_13_R2.generator.NormalChunkGenerator;

/**
 * World generator for CraftBukkit version v1_13_R2.
 *
 * @param <C>
 */
public class ConfigurableGenerator<C extends GeneratorSettings> extends NormalChunkGenerator<C> {

	private static final WorldGenStage.Decoration[] DECORATIONS = WorldGenStage.Decoration.values();

	private final ChunkGenerator generator;
	private final String worldName;
	private WorldConfiguration worldConfig;
	private final Map<Class<? extends BiomeBase>, BiomeBase> biomeCache = new HashMap<>();

	/**
	 * Constructs a new generator instance.
	 *
	 * @param generator the NMS {@link ChunkGenerator}.
	 * @param world     the {@link World} this generator will generate chunks for.
	 * @param seed      the seed of the provided {@link World}.
	 */
	public ConfigurableGenerator(ChunkGenerator generator, World world, long seed) {
		super(world, seed);
		this.worldName = world.getWorldData().getName();
		this.generator = generator;
		this.updateConfig();
	}

	/**
	 * Updates the configuration for this generator and clears all caches.
	 */
	public final void updateConfig() {
		this.worldConfig = Config.WORLDS.get(this.worldName);
		this.biomeCache.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<BiomeBase.BiomeMeta> getMobsFor(EnumCreatureType enumCreatureType, BlockPosition blockPosition) {
		return generator.getMobsFor(enumCreatureType, blockPosition);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BlockPosition findNearestMapFeature(World world, String s, BlockPosition blockPosition, int i, boolean flag) {
		return generator.findNearestMapFeature(world, s, blockPosition, i, flag);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createChunk(IChunkAccess ichunkaccess) {
		generator.createChunk(ichunkaccess);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addFeatures(RegionLimitedWorldAccess regionlimitedworldaccess, WorldGenStage.Features worldgenstage_features) {
		generator.addFeatures(regionlimitedworldaccess, worldgenstage_features);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addDecorations(RegionLimitedWorldAccess regionlimitedworldaccess) {
		BlockFalling.instaFall = true;
		int chunkX = regionlimitedworldaccess.a();
		int chunkZ = regionlimitedworldaccess.b();
		int blockX = chunkX * 16;
		int blockZ = chunkZ * 16;
		BlockPosition blockposition = new BlockPosition(blockX, 0, blockZ);

		BiomeBase biomebase = regionlimitedworldaccess.getChunkAt(chunkX + 1, chunkZ + 1).getBiomeIndex()[0];
		biomeCache.computeIfAbsent(biomebase.getClass(), (key) -> {
			if (this.worldConfig != null) {
				String biomeName = IRegistry.BIOME.getKey(biomebase).b();
				BiomeConfiguration biomeConfig = this.worldConfig.getGlobal();
				if (this.worldConfig.getBiomes() != null && this.worldConfig.getBiomes().containsKey(biomeName))
					biomeConfig = this.worldConfig.getBiomes().get(biomeName);

				if (biomeConfig != null) {
					return new OverriddenBiome(biomebase, biomeConfig);
				}
			}

			return biomebase;
		});

		SeededRandom seededrandom = new SeededRandom();

		for (int n = 0; n < DECORATIONS.length; ++n) {
			WorldGenStage.Decoration currentDecoration = DECORATIONS[n];
			List<WorldGenFeatureComposite<?, ?>> composites = biomebase.a(currentDecoration);

			for (int m = 0; m < composites.size(); m++) {
				WorldGenFeatureComposite<?, ?> composite = composites.get(m);

				seededrandom.b(chunkX, chunkZ, currentDecoration.ordinal());
				composite.a(regionlimitedworldaccess, this, seededrandom, blockposition, WorldGenFeatureConfiguration.e);
			}

		}

		BlockFalling.instaFall = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addMobs(RegionLimitedWorldAccess regionlimitedworldaccess) {
		generator.addMobs(regionlimitedworldaccess);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public C getSettings() {
		return (C) generator.getSettings();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int a(World world, boolean flag, boolean flag1) {
		return generator.a(world, flag, flag1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean canSpawnStructure(BiomeBase biomebase, StructureGenerator<? extends WorldGenFeatureConfiguration> structuregenerator) {
		return generator.canSpawnStructure(biomebase, structuregenerator);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WorldGenFeatureConfiguration getFeatureConfiguration(BiomeBase biomebase, StructureGenerator<? extends WorldGenFeatureConfiguration> structuregenerator) {
		return generator.getFeatureConfiguration(biomebase, structuregenerator);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long2ObjectMap<StructureStart> getStructureStartCache(StructureGenerator<? extends WorldGenFeatureConfiguration> structuregenerator) {
		return generator.getStructureStartCache(structuregenerator);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long2ObjectMap<LongSet> getStructureCache(StructureGenerator<? extends WorldGenFeatureConfiguration> structuregenerator) {
		return generator.getStructureCache(structuregenerator);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WorldChunkManager getWorldChunkManager() {
		return generator.getWorldChunkManager();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getSeed() {
		return generator.getSeed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSpawnHeight() {
		return generator.getSpawnHeight();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGenerationDepth() {
		return generator.getGenerationDepth();
	}

}
