/*
 * GenConfigurator
 * Copyright (C) Floppy012 <https://f012.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package dev.f012.genconfigurator.generator;

import dev.f012.genconfigurator.GenConfigurator;
import dev.f012.genconfigurator.Util;
import java.lang.reflect.Field;
import java.util.Random;
import java.util.logging.Level;
import net.minecraft.server.v1_13_R2.BiomeLayout;
import net.minecraft.server.v1_13_R2.BiomeLayoutOverworldConfiguration;
import net.minecraft.server.v1_13_R2.ChunkGeneratorType;
import net.minecraft.server.v1_13_R2.ChunkProviderGenerate;
import net.minecraft.server.v1_13_R2.ChunkProviderServer;
import net.minecraft.server.v1_13_R2.ChunkTaskScheduler;
import net.minecraft.server.v1_13_R2.GeneratorSettingsOverworld;
import net.minecraft.server.v1_13_R2.IChunkAccess;
import net.minecraft.server.v1_13_R2.WorldChunkManagerOverworld;
import net.minecraft.server.v1_13_R2.WorldServer;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;

/**
 * Generator injector for CraftBukkit v1_13_R2
 */
public class CustomGeneratorInjector extends org.bukkit.generator.ChunkGenerator {

	private ConfigurableGenerator gen;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ChunkData generateChunkData(org.bukkit.World world, Random random, int chunkX, int chunkY, BiomeGrid bg) {
		WorldServer worldServer = ((CraftWorld) world).getHandle();
		if (this.gen == null) {
			ChunkGeneratorType<GeneratorSettingsOverworld, ChunkProviderGenerate> chunkGenType = ChunkGeneratorType.a;
			BiomeLayout<BiomeLayoutOverworldConfiguration, WorldChunkManagerOverworld> biomeLayout = BiomeLayout.c;

			GeneratorSettingsOverworld genSettings = (GeneratorSettingsOverworld) chunkGenType.b();
			BiomeLayoutOverworldConfiguration biomeConfig = ((BiomeLayoutOverworldConfiguration) biomeLayout.b()).a(worldServer.getWorldData()).a(genSettings);
			ChunkProviderGenerate generator = chunkGenType.create(worldServer, biomeLayout.a(biomeConfig), genSettings);

			ConfigurableGenerator fg = new ConfigurableGenerator(generator, worldServer, world.getSeed());
			this.gen = fg;

			try {
				this.inject(worldServer, this.gen);
				GenConfigurator.instance.registerGenerator(this.gen);
			} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException ex) {
				GenConfigurator.instance.getLogger().log(Level.SEVERE, "Error while injecting custom generator into world " + worldServer.getWorldData().getName(), ex);
				throw new RuntimeException("Error while injecting generator!", ex);
			}
		}

		ChunkData data = this.createChunkData(world);
		IChunkAccess access = worldServer.getChunkProvider().a(chunkX, chunkY, true);

		this.gen.createChunk(access);

		return data;
	}

	/**
	 * Injects the provided {@link ConfigurableGenerator} into the provided {@link WorldServer}.
	 *
	 * @param worldServer     the {@link WorldServer} to inject the {@link ConfigurableGenerator} into.
	 * @param customGenerator the {@link CustomGenerator} to inject into the {@link WorldServer}.
	 *
	 * @throws NoSuchFieldException     by {@link Class#getDeclaredField(java.lang.String) }
	 * @throws IllegalArgumentException by {@link Field#get(java.lang.Object) } and {@link Util#definalize(java.lang.reflect.Field) }
	 * @throws IllegalAccessException   by {@link Field#set(java.lang.Object, java.lang.Object) } and {@link Util#definalize(java.lang.reflect.Field) }
	 */
	private void inject(WorldServer worldServer, ConfigurableGenerator customGenerator) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		ChunkProviderServer providerInstance = worldServer.getChunkProvider();

		//Handle replacement in ChunkScheduler
		{
			Field chunkSched = ChunkProviderServer.class.getDeclaredField("chunkScheduler");
			chunkSched.setAccessible(true);

			ChunkTaskScheduler scheduler = (ChunkTaskScheduler) chunkSched.get(providerInstance);
			Field genField = ChunkTaskScheduler.class.getDeclaredField("d"); //private final ChunkGenerator<?> d;

			genField.setAccessible(true); //Set normally accessible
			Util.definalize(genField); //Remove final modifier to allow a new value

			genField.set(scheduler, customGenerator);
		}

		//Handle replacement in ChunkGenerator directly
		{
			Field chunkGen = ChunkProviderServer.class.getDeclaredField("chunkGenerator");

			chunkGen.setAccessible(true); //Set normally accessible
			Util.definalize(chunkGen); //Remvoe final modifier to allow a new value

			chunkGen.set(providerInstance, customGenerator);
		}

		GenConfigurator.instance.getLogger().log(Level.INFO, "Injected generator for world {0}", worldServer.worldData.getName());
	}

}
