/*
 * GenConfigurator
 * Copyright (C) Floppy012 <https://f012.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package dev.f012.genconfigurator.command;

import dev.f012.genconfigurator.GenConfigurator;
import java.util.Locale;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import static dev.f012.genconfigurator.GenConfigurator.sendMessage;

/**
 * Base command class for this plugin.
 */
public class BaseCommand implements CommandExecutor {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmnd, String name, String[] args) {
		if (args.length == 0)
			return false;

		String controlArg = args[0];
		String[] subArgs = new String[args.length - 1];

		if (args.length > 1)
			System.arraycopy(args, 1, subArgs, 0, args.length);

		switch (controlArg.toLowerCase(Locale.ENGLISH)) {
			case "reload":
				return this.reloadConfig(sender, subArgs);
			default:
				return false;
		}
	}

	/**
	 * Internal method for the reload sub-arg.
	 *
	 * @param sender the {@link CommandSender} which executed the command.
	 * @param args   the arguments provided for this sub-arg. May be empty.
	 *
	 * @return whether the command was executed successfully.
	 */
	private boolean reloadConfig(CommandSender sender, String[] args) {
		GenConfigurator.instance.reloadConfig();
		sendMessage(sender, "§aConfiguration reloaded!");
		return true;
	}

}
